Source: nifticlib
Maintainer: NeuroDebian Team <team@neuro.debian.net>
Uploaders: Michael Hanke <mih@debian.org>,
           Yaroslav Halchenko <debian@onerussian.com>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 11~),
               cmake,
               zlib1g-dev,
               doxygen,
               python,
               graphviz,
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/neurodebian-team/nifticlib
Vcs-Git: https://salsa.debian.org/neurodebian-team/nifticlib.git
Homepage: http://niftilib.sourceforge.net

Package: libnifti2
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: IO libraries for the NIfTI-1 data format
 Niftilib is a set of i/o libraries for reading and writing files in the 
 NIfTI-1 data format. NIfTI-1 is a binary file format for storing medical 
 image data, e.g. magnetic resonance image (MRI) and functional MRI (fMRI) 
 brain images.
 .
 This package contains the shared library of the low-level IO library niftiio,
 low-level IO library znzlib and the nifticdf shared library that provides
 functions to compute cumulative distributions and their inverses.

Package: libnifti-dev
Architecture: any
Section: libdevel
Depends: libnifti2 (= ${binary:Version}),
         ${misc:Depends}
Conflicts: libnifti1-dev,
           libniftiio-dev,
           libnifti0-dev,
           libfslio-dev
Replaces: libnifti1-dev
Description: IO libraries for the NIfTI-1 data format
 Niftilib is a set of i/o libraries for reading and writing files in the 
 NIfTI-1 data format. NIfTI-1 is a binary file format for storing medical 
 image data, e.g. magnetic resonance image (MRI) and functional MRI (fMRI) 
 brain images.
 .
 This package provides the header files and static libraries of libniftiio,
 znzlib and libnifticdf.

Package: nifti-bin
Architecture: any
Section: utils
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: tools shipped with the NIfTI library
 Niftilib is a set of i/o libraries for reading and writing files in the 
 NIfTI-1 data format. NIfTI-1 is a binary file format for storing medical 
 image data, e.g. magnetic resonance image (MRI) and functional MRI (fMRI) 
 brain images.
 .
 This package provides the tools that are shipped with the library 
 (nifti_tool, nifti_stats and nifti1_test).

Package: libnifti-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         libjs-jquery
Description: NIfTI library API documentation
 Niftilib is a set of i/o libraries for reading and writing files in the 
 NIfTI-1 data format. NIfTI-1 is a binary file format for storing medical 
 image data, e.g. magnetic resonance image (MRI) and functional MRI (fMRI) 
 brain images.
 .
 This package provides the library API reference documentation.
